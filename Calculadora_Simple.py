#!/usr/bin/env python3
#
# Copyright 2017 Lawrence José González Delgado
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Este programa es una calculadora que se usa desde la terminal."""


def es_float(var):
    """Distingue si var es float u otro tipo y devuelve bool."""
    try:
        if var == float(var):
            return True
    except Exception:
        return False


def error(var):
    """Si var cumple la condición se pedirá su reintroducción."""
    while es_float(var) is False:
        var = input('\nSolo puede introducir números: ')
    return float(var)


def int_float(var):
    """Determina si var puede ser un número entero o no."""
    return int(var) if var.is_integer() is True else var


# Variables de texto y variables modificables durante la ejecución.
menu = (
    '\n#####################'
    '\n# Menú de Opciones  #'
    '\n# 0: Salir.         #'
    '\n# 1: Sumar.         #'
    '\n# 2: Restar.        #'
    '\n# 3: Multiplicar.   #'
    '\n# 4: Dividir.       #'
    '\n#####################'
    '\n'
    '\nIntroduzca el número de la operación que desea realizar: '
)

# El programa seguirá ejecutándose hasta que el usuario diga que quiere
# salir.
while True:
    opc = input('{}'.format(menu))
    # Si el argumento introducido es invalido se pedirá que vuelva a ser
    # introducido.
    while opc not in (['0', '1', '2', '3', '4']):
        opc = input('\nEl argumento es inválido.\n{}'.format(menu))
    if opc == '0':
        break

    num_1 = error(input('\nIntroduzca un número: '))
    num_2 = error(input('\nIntroduzca otro número: '))
    # Evita que se realice una división o resta entre 0.
    while opc in ('2', '4') and num_2 == 0:
        num_2 = error(input('\nNo puede realizar divisiones entre 0: '))

    # Se realizara la operación y se cambiara el signo según la opción
    # tomada por el usuario.
    if opc == '1':
        res = float(num_1) + float(num_2)
        sign = '+'
    elif opc == '2':
        res = float(num_1) - float(num_2)
        sign = '-'
    elif opc == '3':
        res = float(num_1) * float(num_2)
        sign = '*'
    elif opc == '4':
        res = float(num_1) / float(num_2)
        sign = '/'

    # Si res no es decimal entonces se mostrara como número entero.
    print('\n{} {} {} = {}'.format(
        int_float(num_1), sign, int_float(num_2), int_float(res)
    ))
