#!/usr/bin/env python3
#
# Copyright 2017 Lawrence José González Delgado
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Esto es otro juego de piedra, papel y tijera, pero en este caso es
computadora vs. computadora hasta que se alcance el número de partidas
que el usuario haya designado.
"""

import random


def percent(var):
    """Se calcula el porcentaje del número entrante"""
    return var * 100 / times


def ppt():
    """Se elige aleatóriamente una de las opciones y se devuelve."""
    return random.choice((1, 2, 3))


def is_int(var):
    """Distingue si var es int u otro tipo y devuelve bool."""
    try:
        int(var)
        return True
    except ValueError:
        return False


tie = 0
score_1 = 0
score_2 = 0
score_compu = '\nComputadora {} = {}'
percent_compu = '\nPorcentaje de la Computadora {} = {}%'

print(
    'Piedra, Papel o Tijera.' +
    '\n'
    '\nEste es un juego de computadora contra computadora para ver el ' +
    'porcentaje de partidas ganadas, perdidas y empates.'
)
times = input(
    '\nEscriba la cantidad de partidas que quiera que la computadora haga: '
)
# Se comprueba si el usuario ha escrito un argumento invalido, si no lo
# hizo entonces se pedirá que lo reintroduzca.
while is_int(times) is False or int(times) <= 0:
    times = input(
        '\nSolo puede escribir números, estos deben de ser enteros y mayores '
        + 'a 0: '
    )
times = int(times)

print('\n¡La Computadora vs. la Computadora!\nJugaran {} {}.'.format(
    times, 'rondas' if times > 1 else 'ronda'
))

# Se hacen tantas partidas como el jugador haya indicado.
for i in range(1, times + 1):
    # Se hacen las elecciones entre piedra, papel o tijera.
    elec_1 = ppt()
    elec_2 = ppt()
    # Se decide quien es el ganador o si hay empate.
    if elec_1 == elec_2:
        tie += 1
    elif (
        elec_1 == 1 and elec_2 == 2
        or elec_1 == 2 and elec_2 == 3
        or elec_1 == 3 and elec_2 == 1
    ):
        score_2 += 1
    else:
        score_1 += 1

# Se muestran las estadísticas de las partidas jugadas.
print(
    score_compu.format('1', score_1)
    + score_compu.format('2', score_2)
    + '\nEmpates = {}'.format(tie)
    + percent_compu.format('1', percent(score_1))
    + percent_compu.format('2', percent(score_2))
    + '\nPorcentaje de empates = {}%'.format(percent(tie))
)
